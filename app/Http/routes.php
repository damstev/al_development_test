<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', 'WelcomeController@index');
Route::get('/', function(){
    return view('index');
});

Route::get('/api/searchActor/{name}', 'ApiController@searchActor');
Route::get('/api/searchAll/{name}', 'ApiController@searchAll');
Route::get('/api/getActor/{id}', 'ApiController@getActor');
Route::get('/api/getMovie/{id}', 'ApiController@getMovie');
Route::get('/api/getTv/{id}', 'ApiController@getTv');
