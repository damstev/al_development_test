<?php

namespace App\Http\Controllers;

use Request;
use Tmdb\Laravel\Facades\Tmdb;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * make a search for an actor.
     *
     * @return Response
     */
    public function searchActor($name)
    {
        $page = Request::input('page');
        $options = [];
        if ($page) {
            $options['page'] = $page;
        }
        $rs = Tmdb::getSearchApi()->searchPersons($name, $options);

        return response()->json($rs);
    }

    /**
     * make a search for an actor, movies and tv.
     *
     * @return Response
     */
    public function searchAll($name)
    {
        $page = Request::input('page');
        $options = [];
        if ($page) {
            $options['page'] = $page;
        }
        $rs = Tmdb::getSearchApi()->searchMulti($name, $options);

        return response()->json($rs);
    }

    /**
     * Get the actor information plus his movies.
     *
     * @return Response
     */
    public function getActor($id)
    {
        $options = [
      'append_to_response' => 'movie_credits',
    ];
        $rs = Tmdb::getPeopleApi()->getPerson($id, $options);

        return response()->json($rs);
    }

    /**
     * Get the movie information.
     *
     * @return Response
     */
    public function getMovie($id)
    {
        $options = [
      'append_to_response' => 'credits',
    ];
        $rs = Tmdb::getMoviesApi()->getMovie($id, $options);

        return response()->json($rs);
    }

    /**
     * Get the TV information.
     *
     * @return Response
     */
    public function getTv($id)
    {
        $options = [
      'append_to_response' => 'credits',
    ];
        $rs = Tmdb::getTvApi()->getTvshow($id, $options);

        return response()->json($rs);
    }
}
