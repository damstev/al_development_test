angular.module( "uiDeveloperTestApp" )
.directive('listResultItem', function() {
  return {
    templateUrl: "views/templates/result.template.html"
  };
});
