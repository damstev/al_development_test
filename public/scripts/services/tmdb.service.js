'use strict';

/**
* @ngdoc service
* @name uiDeveloperTestApp.theMovieDB
* @description
* # theMovieDB
* Factory in the uiDeveloperTestApp.
*/
angular.module('uiDeveloperTestApp')
.factory('tmdbService', function ($resource, $http, $q) {

  return {
    person_search: function (name, page) {
      var canceller = $q.defer();

      var cancel = function(reason){
        canceller.resolve(reason);
      };
      var url = _baseUrl + '/api/searchActor/'+name;
      var promise = $http.get(url, {params: {page: page}, timeout: canceller.promise});

      return {
            promise: promise,
            cancel: cancel
        };
    },

    generic_search: function (name, page) {
      var canceller = $q.defer();

      var cancel = function(reason){
        canceller.resolve(reason);
      };
      var url = _baseUrl + '/api/searchAll/'+name;
      var promise = $http.get(url, {params: {page: page}, timeout: canceller.promise});

      return {
            promise: promise,
            cancel: cancel
        };
    },

    person_get: function (id) {
      var url = _baseUrl + '/api/getActor/'+id;
      return $http.get(url);
    },

    movie_get: function (id) {
      var url = _baseUrl + '/api/getMovie/'+id;
      return $http.get(url);
    },

    tv_get: function (id) {
      var url = _baseUrl + '/api/getTv/'+id;
      return $http.get(url);
    },

    search_term: '',
    cached_search: [],
  };
});
