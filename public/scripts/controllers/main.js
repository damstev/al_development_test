'use strict';

/**
* @ngdoc function
* @name uiDeveloperTestApp.controller:MainCtrl
* @description
* # MainCtrl
* Controller of the uiDeveloperTestApp
*/
angular.module('uiDeveloperTestApp')
.controller('MainCtrl', ['$scope', 'tmdbService', function ($scope, tmdbService) {
  $scope.sSearchTerm = tmdbService.search_term || '';
  $scope.aoResult = tmdbService.cached_search || [];
  $scope.currentPage = 1;
  $scope.totalPages = 1;
  var currentRequest = null;

  $scope.search = function () {
    tmdbService.search_term = $scope.sSearchTerm;
    if (!$scope.sSearchTerm || $scope.sSearchTerm.length < 3){
      $scope.aoResult = [];
      return;
    }


    if(tmdbService.cached_search[$scope.sSearchTerm]){
      $scope.aoResult = tmdbService.cached_search[$scope.sSearchTerm];
      return;
    }


    $scope.currentPage = 1;
    $scope.totalPages = 1;

    if(currentRequest)
    currentRequest.cancel('Another request is on the way.');

    $scope.searchLoading = true;
    currentRequest = tmdbService.generic_search($scope.sSearchTerm, 1);
    currentRequest.promise.then(function (result) {
      $scope.aoResult = result.data.results;
      $scope.totalPages = result.data.total_pages;
      $scope.searchLoading = false;
    });
  }

  $scope.clean = function () {
    $scope.sSearchTerm = '';
    $scope.aoResult = [];
    tmdbService.search_term= '';
  }

  $scope.loadMore = function(){


    $scope.loadingMore = true;

    if($scope.currentPage <= $scope.totalPages && $scope.sSearchTerm != ''){
      $scope.currentPage++;
      tmdbService.generic_search($scope.sSearchTerm, $scope.currentPage).promise.then(function (result) {
        $scope.aoResult =   $scope.aoResult.concat(result.data.results);
        $scope.loadingMore = false;
      });
    }else{
      $scope.loadingMore = false;
    }
  }

  $scope.search();

  $scope.$watch('aoResult', function (val) {
    tmdbService.cached_search[$scope.sSearchTerm] = val;
  });

}]);
