'use strict';

angular.module('uiDeveloperTestApp')
        .controller('TvCtrl', ['$scope', '$routeParams', 'tmdbService', '$filter', function ($scope, $routeParams, tmdbService, $filter) {

                var id = $routeParams.id;

                $scope.oTv = {};
                $scope.aoCredits = {};
                $scope.loading = true;
                tmdbService.tv_get(id).then(function (result) {
                    $scope.oTv = result.data;
                    $scope.aoCredits = result.data.credits.cast.concat(result.data.credits.crew);
                    $scope.loading = false;
                });

            }]);
