'use strict';

angular.module('uiDeveloperTestApp')
        .controller('MovieCtrl', ['$scope', '$routeParams', 'tmdbService', '$filter', function ($scope, $routeParams, tmdbService, $filter) {

                var id = $routeParams.id;

                $scope.oMovie = {};
                $scope.aoCredits = {};
                $scope.loading = true;
                tmdbService.movie_get(id).then(function (result) {
                    $scope.oMovie = result.data;
                    $scope.aoCredits = result.data.credits.cast.concat(result.data.credits.crew);
                    $scope.loading = false;
                });

            }]);
