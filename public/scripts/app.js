'use strict';

/**
 * @ngdoc overview
 * @name uiDeveloperTestApp
 * @description
 * # uiDeveloperTestApp
 *
 * Main module of the application.
 */
angular
        .module('uiDeveloperTestApp', [
            'ngAnimate',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'angular-ladda',
            'infinite-scroll',
        ])
        .config(function ($routeProvider, laddaProvider) {
            $routeProvider
                    .when('/search/', {
                        templateUrl: 'views/main.html',
                        controller: 'MainCtrl'
                    })
                    .when('/detail/:id', {
                      templateUrl: 'views/detail.html',
                      controller: 'DetailCtrl'
                    })
                    .when('/movie/:id', {
                      templateUrl: 'views/movie.html',
                      controller: 'MovieCtrl'
                    })
                    .when('/tv/:id', {
                      templateUrl: 'views/tv.html',
                      controller: 'TvCtrl'
                    })
                    .otherwise({
                        redirectTo: '/search'
                    });

            laddaProvider.setOption({
              style: 'expand-left'
            });
        });
