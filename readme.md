## Alert Logic Senior Developer Test
This project pretends to fulfill the requirements presented in the UI Developer Test.
This is a web application that allows you to search for actors, movies and tv series using ["The Movie Database" API](https://www.themoviedb.org/documentation/api).

## Technical Information

This project was developed using the next frameworks, libraries, and services:
- [The movie database](https://www.themoviedb.org/documentation/api)
- [Placehold it](http://placehold.it/)
- [Laravel 5](http://laravel.com/)
- [php-tmdb/laravel ](https://github.com/php-tmdb/laravel)
- [Bootstrap](http://getbootstrap.com/)
- [Laravel 5 ](http://laravel.com/)
- [Angular](https://angularjs.org/)


## Installation
You need to have [composer](https://getcomposer.org/) and [bower](http://bower.io/) already installed.
#### Clone the repository
```
git clone https://bitbucket.org/damstev/al_development_test
```
#### Install the backend dependencies
```
cd al_development_test
composer install
```
#### Install the frontend dependencies
```
cd public
bower install
```
#### Run the project
```
cd ../
php artisan serve
```
#### Open the browser and go to the URL: [http://localhost:8000/](http://localhost:8000/)
